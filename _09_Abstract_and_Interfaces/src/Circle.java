// Класс Circle - должен являться потомком класса Ellipse
public class Circle extends Ellipse implements Moveable {
    public Circle(int x, int y) {super(x, y);}

    private double d;

    public double getD() {
        return d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public void getPerimeter(){
        //double L = 3.14 * getD();
        System.out.println("Perimeter Circle = " + (double)(3.14 * getD()));
    }

    public void move(int x, int y) {
        int temp_x = (int) (Math.random() *10);
        int temp_y = (int) (Math.random() *10);

        this.setX(temp_x);
        this.setY(temp_y);
        System.out.println("Переместили круг по новым координатам: x=" + this.getX() +" y=" + this.getY());
    }
}
