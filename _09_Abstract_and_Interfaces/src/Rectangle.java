// Класс Rectangle должен быть потомком класса Figure.
public class Rectangle extends Figure{
    private int a;
    private int b;

    public Rectangle(int x, int y) {super(x, y);}

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public void getPerimeter(){
        System.out.println("Perimeter Rectangle = " + (getA()+getB())*2);
    }
}
