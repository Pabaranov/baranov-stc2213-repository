// Класс Ellipse должен быть потомком класса Figure.
public class Ellipse extends Figure{

   private int a;
   private int b;

   public void setA(int a) {
      this.a = a;
   }

   public void setB(int b) {
      this.b = b;
   }

   public int getA() {
      return a;
   }

   public int getB() {
      return b;
   }

   public Ellipse(int x, int y) {super(x, y);}

   public void getPerimeter(){
      double P = ((3.14 * getA()*getB() + (getA() - getB()))/(getA()+getB()))*4;
      System.out.println("Perimeter Ellipse = " + P);
   }
}
