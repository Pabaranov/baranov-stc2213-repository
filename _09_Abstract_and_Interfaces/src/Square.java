// Класс Square - должен являться потомком класса Rectangle
public class Square extends Rectangle implements Moveable
//public class Square implements Moveable
{
    private int a;

    public Square(int x, int y) {super(x, y);}

    @Override
    public int getA() {
        return a;
    }

    @Override
    public void setA(int a) {
        this.a = a;
    }

    public void getPerimeter(){
        System.out.println("Perimeter Square = " + (getA()*4));
    }

    public void move(int x, int y) {
        int temp_x = (int) (Math.random() *10);
        int temp_y = (int) (Math.random() *10);

        this.setX(temp_x);
        this.setY(temp_y);
        System.out.println("Переместили квадрат по новым координатам: x=" + this.getX() +" y=" + this.getY());
    }
}
