public class main {
    public static void main(String[] args) {
       Rectangle rectangle = new Rectangle(1, 1);
       rectangle.setA(10);
       rectangle.setB(5);

       Square square = new Square(2, 4);
       square.setA(10);

       Ellipse ellipse = new Ellipse(1,1);
       ellipse.setA(3);
       ellipse.setB(1);

       Circle circle = new Circle(6, 10);
       circle.setD(4);

        rectangle.getPerimeter();
        square.getPerimeter();
        ellipse.getPerimeter();
        circle.getPerimeter();
        square.move(square.getX(), square.getY());
        circle.move(circle.getX(), circle.getY());
    }
}
