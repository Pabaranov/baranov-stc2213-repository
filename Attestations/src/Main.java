
public class Main {
    public static void main(String[] args) {
        UsersRepositoryFileImpl usersRepositoryFile = new UsersRepositoryFileImpl();
        User user;

        // Ищем пользователя по ID
        user = usersRepositoryFile.findById(2);
        if (user.getName() == null)
            System.out.println("Пользователь не найден");
        else {
            System.out.print("Пользоватедь " + user.getName() + " присутствует, его возраст " + user.getAge());
            if (user.isWorker())
                System.out.println(", он работает");
            else
                System.out.println(", он безработный");

            // Меняем данные пользователя и перезаписываем его
            user.setName("Vladimir");
            user.setAge(36);
            usersRepositoryFile.update(user);
        }

        // Добавляем нового пользователя
        user.setId(5);
        user.setName("Fedot");
        user.setLastName("Muhin");
        user.setAge(26);
        user.setWorker(true);
        usersRepositoryFile.create(user);

        // Удаляем пользоватедя по ID
        if (!usersRepositoryFile.delete(3))
            System.out.println("Невозможно удалить пользователя");
    }
}
