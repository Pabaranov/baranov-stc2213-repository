public class User {
    private int id;
    private String name;
    private String lastName;
    private int age;
    private boolean worker;

    public User(int id, String name, String lastName, int age, boolean worker) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.worker = worker;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorker() {
        return worker;
    }

    @Override
    public String toString() {
        return id + "|" + name + "|" + lastName + "|" + age;
    }

    public void setWorker(boolean worker) {
        this.worker = worker;
    }
}
