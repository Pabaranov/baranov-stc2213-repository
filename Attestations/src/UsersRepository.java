public interface UsersRepository {
    User findById(int data_id);

    User update(User user);

    User create(User user);

    boolean delete(int id);
}
