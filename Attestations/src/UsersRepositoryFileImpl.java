import java.io.*;

public class UsersRepositoryFileImpl implements UsersRepository {
    @Override
    public User findById(int data_id) {
        String line;
        User user = new User();

        try (BufferedReader reader = new BufferedReader(new FileReader("Test.txt"))) {
            //User user = new User();

            while ((line = reader.readLine()) != null) {
                String[] info = line.split("\\|");
                if (Integer.parseInt(info[0]) == data_id) {
                    user.setId(Integer.parseInt(info[0]));
                    user.setName(info[1]);
                    user.setLastName(info[2]);
                    user.setAge(Integer.parseInt(info[3]));
                    user.setWorker(Boolean.parseBoolean(info[4]));
                }
            }
            return user;
        } catch (IOException e) {
            //throw new RuntimeException();
            return null;
        }
    }

    @Override
    public User update(User user) {
        String line;
        boolean temp = false;

        try (BufferedReader reader = new BufferedReader(new FileReader("Test.txt"))) {
            while ((line = reader.readLine()) != null) {
                String[] info = line.split("\\|");
                if (Integer.parseInt(info[0]) == user.getId()) {
                    line = user.toString();
                }

                if (!userWriter(line, temp))
                    return null;
                temp = true;
            }
        } catch (IOException e) {
            //throw new RuntimeException();
            return null;
        }
        return user;
    }

    @Override
    public User create(User user) {
        if (!userWriter(user.toString(), true))
            return null;
        return user;
    }

    @Override
    public boolean delete(int id) {
        String line;
        boolean temp = false;
        boolean result = false;

        try (BufferedReader reader = new BufferedReader(new FileReader("Test.txt"))) {
            while ((line = reader.readLine()) != null) {
                String[] info = line.split("\\|");
                if (Integer.parseInt(info[0]) != id) {
                    userWriter(line, temp);
                    temp = true;
                } else {
                    result = true;
                }
            }
        } catch (IOException e) {
            //throw new RuntimeException();
            return false;
        }
        return result;
    }

    public static boolean userWriter(String line, boolean temp) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("Test.txt", temp))) {
            writer.write(line);
            writer.write("\n");
            writer.flush();
        } catch (IOException e) {
            //throw new RuntimeException();
            return false;
        }
        return true;
    }
}
