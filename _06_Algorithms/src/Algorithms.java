import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

// На вход подается последовательность чисел, стремящаяся к бесконечности (массив source)
// Необходимо вывести число, которое присутствует в последовательности минимальное количество раз.
public class Algorithms {
    public static void main(String[] args) {
        int [] source = {1, 2, 3, 2, 3, 1, 6, 3};
        int [] tempArray = new int[source.length];
        int min = 0;
        int res = 0;
        for(int i=0; i < source.length; i++) {
            for(int j=0; j < source.length; j++) {
                if(source[i] == source[j])
                    tempArray[i] += 1;
            }
            if(i > 0) {
                if (tempArray[i] < min) {
                    min = tempArray[i];
                    res = source[i];
                }
            }
            else {
                min = tempArray[i];
                res = source[i];
            }
        }
        System.out.println(res);
    }
}
