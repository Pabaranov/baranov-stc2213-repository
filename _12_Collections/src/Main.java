import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        String s = new String("Маша и три медведя Маша");

        Map<String, Integer> cntWords = new HashMap<>();

        //Pattern pattern = Pattern.compile("' '{2,}");
        Pattern pattern = Pattern.compile(" ");

        String[] words = pattern.split(s);

        for (int i = 0; i < words.length; i++) {

            if(cntWords.containsKey(words[i])) {
                int temp = cntWords.get(words[i]);
                temp++;
                cntWords.put(words[i], temp);
            } else {
                cntWords.put(words[i], 1);
            }

//            // Тоже самое, только через метод compute
//            cntWords.compute(words[i], (k, v) -> {
//                if (v == null) v = 0;
//                return v += 1;
//            });
        }

        cntWords.forEach((key, value) -> System.out.println(key + " - " + value));
    }
}
