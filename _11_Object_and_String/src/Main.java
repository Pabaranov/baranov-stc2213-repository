
public class Main {
    public static void main(String[] args) {
        Human[] human = new Human[3];

        for (int i = 0; i < human.length; i++) {
            human[i] = new Human("Иван_" + i, "Иванов_" + i, "Петрович_" + i,
                    "Джава", "Программистов", Integer.toString(10), Integer.toString(30 + i),
                    "9822 8976" + Integer.toString(40 + i));
        }

        //Второму человеку, задаём такой-же номер паспорта, как и первому
        human[1].setNumberPassport("9822 897640");

        for (int i=0; i < human.length; i++) {
            System.out.println(human[i].toString());
        }

        for (int i = human.length -1; i != 0; i--) {
            if(human[i].equals(human[i-1]))
                System.out.println(human[i].getLastName()+" и "+human[i-1].getLastName()+
                        " имеют идентичные номера паспорта");
            else
                System.out.println(human[i].getLastName()+" и "+human[i-1].getLastName()+
                        " имеют различные номера паспорта");
        }

    }
}
