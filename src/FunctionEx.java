import com.sun.xml.internal.bind.v2.runtime.output.StAXExStreamWriterOutput;

import java.util.Arrays;

public class FunctionEx {
    public static void main(String[] args) {
        int[] array;
        int x;
        array = new int[] {2, 4, 8, 12, 20};
        x = returnIndex(array, 8);
        System.out.println(x);
        x = returnIndex(array, 10);
        System.out.println(x);

        array = new int[] {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        shiftArrayLeft(array);
        System.out.println(Arrays.toString(array));
    }

    //функция, принимающая на вход массив и целое число.
    // Данная функция должна вернуть индекс этого числа в массиве. Если число в массиве отсутствует - вернуть -1.
    public static int returnIndex(int[] array, int i) {
        for(int x=0; x< array.length; x++) {
            if(array[x] == i)
                return x;
        }
        return -1;
    }

    //процедура, перемещающая все значимые элементы влево, заполнив нулевые, например:
    //Было:  34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
    //Стало: 34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
//    public static void shiftArrayLeft(int[] array) {
//        int x;
//        int c=0;
//        int length = array.length;
//        int[] tempArray = new int[length];
//        for(x=0; x< length; ) {
//            if (array[x] == 0)
//            {
//                System.arraycopy(array, x + 1, tempArray, 0, length - (x +1));
//
//                System.arraycopy(tempArray, 0, array, x, length - (x +1));
//                array[length - 1] = 0;
//            }
//            else x++;
//            if(++c == length -1)    return;
//        }
//    }

    public static void shiftArrayLeft(int[] array) {
        int c=0;
        int length = array.length;
        int[] tempArray = new int[length];
        for(int x=0; x< length; x++) {
            if (array[x] != 0) {
                tempArray[c++] = array[x];
            }
        }
        System.arraycopy(tempArray, 0, array, 0, length);
    }
}
