import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int randomValue = random.nextInt(10) +1;
        // Создаем массив класса Human, на случайное кол-во ячеек
        Human[] humans = new Human[randomValue];

//        System.out.println(randomValue);
//        System.out.println("******");

        //заполняем массив объектами класса Human, со случайными значениями поля age
        for(int i=0; i < humans.length; i++) {
            randomValue = random.nextInt(100);
            humans[i] = new Human("Name_" +i, "LastName_" +i, randomValue);

            System.out.println(humans[i].getName() +" " + humans[i].getLastName() +" " + humans[i].getAge());
        }
        System.out.println("**********************");

        //Сортируем массив по возрасту (проверяя поле age)
        Human[] tempHumans = new Human[1];
        for (int i = 0; i < humans.length; i = i + 1) {
            for (int j = 0; j < humans.length - 1; j = j + 1) {
                if (humans[j].getAge() > humans[j + 1].getAge()) {
                    tempHumans[0] = humans[j];
                    humans[j] = humans[j +1];
                    humans[j + 1] = tempHumans[0];
                }
            }
        }

        //Выводим результат в консоль
        for(int i=0; i < humans.length; i++) {
            System.out.println(humans[i].getName() +" " + humans[i].getLastName() +" " + humans[i].getAge());
        }
    }
}
