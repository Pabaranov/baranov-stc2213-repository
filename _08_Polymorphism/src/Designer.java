public class Designer extends Worker{
    public Designer(String name, String lastName, String  profession) {
        super(name, lastName, profession);
    }

    public void goToWork() {
        System.out.println(getName() +" " +getLastName() +" - " +getProfession() +" - работает творчески");
    }
}
