public class main {
    public static void main(String[] args) {
        Worker[] workers = new Worker[4];

        workers[0] = new Designer("Вася", "Иванов", "Дизайнер");
        workers[1] = new Engineer("Иван", "Смирнов", "Инженер");
        workers[2] = new Tester("Вова", "Пупкин", "Тестировщик");
        workers[3] = new Programmer("Евгения", "Полякова", "Программист");

        for (int i=0; i < workers.length; i++) {
            workers[i].goToWork();
            workers[i].goToVacation(14+(i*2));
        }
    }
}
