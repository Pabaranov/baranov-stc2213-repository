public class Programmer extends Worker{
    public Programmer(String name, String lastName, String  profession) {
        super(name, lastName, profession);
    }

    public void goToWork() {
        System.out.println(getName() +" " +getLastName() +" - " +getProfession() +" - пишет код");
    }
}
