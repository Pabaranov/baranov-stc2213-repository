public class Tester extends Worker{
    public Tester(String name, String lastName, String  profession) {
        super(name, lastName, profession);
    }

    public void goToWork() {
        System.out.println(getName() +" " +getLastName() +" - " +getProfession() +" - тестирует прикладное ПО");
    }
}
