public class Worker {
    private String  name;
    private String  lastName;
    private String  profession;

    public Worker(String name, String lastName, String  profession) {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
    }

    public Worker() {}

    public void goToWork() {
        System.out.println(name +" " +lastName +" - " +profession +" - работает не понятно как");
    }

    public void goToVacation(int days) {
        System.out.println("   уходит в отпуск на " + days +" дней" );
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getProfession() {
        return profession;
    }
}
