import java.sql.Array;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int data = 6;
        printNumbers(data, condition -> condition % 2 == 0);

        data = 147;
        checkSumNumbers(data, condition -> condition % 2 == 0);
    }

    public static void printNumbers(int data, ByCondition condition) {
            if(condition.isOk(data))
                System.out.println(data + " число чётное");
            else
                System.out.println(data + " число не чётное");
    }

    public static void checkSumNumbers(int data, ByCondition condition) {
        int temp = 0;

        while (data >0) {
            temp += data % 10;
            data = data / 10;
        }

        if(condition.isOk(temp))
            System.out.println("сумма чисел введённого числа = " + temp + " - она чётная");
        else
            System.out.println("сумма чисел введённого числа = " + temp + " - она не чётная");
    }
}
