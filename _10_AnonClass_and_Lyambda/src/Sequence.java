import java.util.Arrays;

public class Sequence {
    public static void main(String[] args) {
        int[] array = {1,1,3,2,3,6,10,7,9};

        ByCondition condition = new ByCondition() {
            @Override
            public boolean isOk(int number) {
                return number % 3 == 0;
            }
        };

        System.out.println("Печатаем числа удовлетворяющие выражению заданному в condition");
        System.out.println(Arrays.toString(filter(array, condition)));
    }

    //метод должен возвращать массив, который содержит элементы, удовлетворяющиие логическому выражению в condition
    public static int[] filter(int[] array, ByCondition condition) {
//        int[] tempArray = new int[array.length];
//        int j = 0;
//        for (int i=0; i < array.length; i++){
//            if(condition.isOk(array[i]))
//                tempArray[j++] = array[i];
//        }
//        int[] tempArray2 = new int[j];
//        for( j = j-1; j != -1; j--)
//            tempArray2[j] = tempArray[j];
//        return tempArray2;

        return Arrays.stream(array).filter(i -> condition.isOk(i)).toArray();
    }
}
